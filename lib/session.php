<?php
/*
 * Fonctions pour la gestion de la session
 */
function is_connect(){
    // Vérifier que l'utilisateur est connecté
    if(isset($_SESSION['connect'])){
        if($_SESSION['connect']){
            return true;
        }
    }
    return false;
}

function connect($id){
    // Connecter l'utilisateur
    $_SESSION['connect'] = true;
    $_SESSION['id'] = $id;
}

function deconnect(){
    $_SESSION['connect'] = false;
    $_SESSION['id'] = null;
}

function btnConnexion(){
    // Charger les bouttons de connexion ou de deconnexion
    if(is_connect()){
        ob_start();
        include 'view/inc/btnConnect.php';
        $btnConnect = ob_get_contents();
        ob_end_clean();
    }else{
        ob_start();
        include 'view/inc/btnNotConnect.php';
        $btnConnect = ob_get_contents();
        ob_end_clean();
    }
    return $btnConnect;
}