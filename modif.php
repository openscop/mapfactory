<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// echo 'test';
require_once('begin.inc.php');
if(!is_connect()){
    header('Location:index.php');
    exit;
}

//Création liste Taxonomie
$taxonomy = new Taxonomy;
$tabTaxonomy = $taxonomy->getLevel0();


ob_start();
foreach ($tabTaxonomy as $list){
    include 'view/form/listTaxonomy.php';
}
$listTaxonomy = ob_get_contents();
ob_end_clean();
ob_start();
foreach ($tabTaxonomy as $list){
    include 'view/form/optionTaxonomy.php';
}
$optionTaxonomy = ob_get_contents();
ob_end_clean();
//Création liste layer
$layer = new Layer;
$tabLayer = $layer->SelectAll();



ob_start();
foreach ($tabLayer as $id_taxo => $table){
   echo "<div data-idTaxo ='$id_taxo' class='layerByTaxo'>";
   echo "<h4 id='taxoTitle-".$id_taxo."'>".$table['titre']."</h4>";
   foreach ($table['layer'] as $layer){
       include 'view/form/listLayer.php';
   }
   echo '</div>';
}
$listLayer = ob_get_contents();
ob_end_clean();
// Création liste utilisateur
$user = new User;
$tabUser = $user->liste();
ob_start();
foreach ($tabUser as $id => $name){
    include 'view/form/listUser.php';
}
$listUser= ob_get_contents();
ob_end_clean();
include 'view/form/main.php';
