<?php
header('Content-Type: application/json');

$id_layer = $_GET['idl'];

/*
$query = 'http://overpass-api.de/api/interpreter?data=';
$query .= '%5Bout%3Ajson%5D%3B';
$query .= 'area%5B%22wikipedia%22%3D%22fr%3ASaint%2D%C3%89tienne%22%5D%2D%3E%2Ea%3B%28node%5B%22amenity%22%3D%22potty%5Farea%22%5D%28area%2Ea%29%3Bway%5B%22amenity%22%3D%22potty%5Farea%22%5D%28area%2Ea%29%3Brelation%5B%22amenity%22%3D%22potty%5Farea%22%5D%28area%2Ea%29%3Bnode%5B%22leisure%22%3D%22dog%5Fpark%22%5D%28area%2Ea%29%3Bway%5B%22leisure%22%3D%22dog%5Fpark%22%5D%28area%2Ea%29%3Brelation%5B%22leisure%22%3D%22dog%5Fpark%22%5D%28area%2Ea%29%3B%29%3Bout%20body%3B%3E%3Bout%20skel%20qt%3B%0A';
*/

$url = 'http://overpass-api.de/api/interpreter?data=';
$url .= urlencode('[out:json];');

switch ($id_layer) {
    case 1:
        $url .= urlencode('
area["wikipedia"="fr:Saint-Étienne"]->.a;
(
  node
    ["amenity"="potty_area"]
    (area.a);
  way
    ["amenity"="potty_area"]
    (area.a);
  relation
    ["amenity"="potty_area"]
    (area.a);
  node
    ["leisure"="dog_park"]
    (area.a);
  way
    ["leisure"="dog_park"]
    (area.a);
  relation
    ["leisure"="dog_park"]
    (area.a);
);
out body;
>;
out skel qt;
        ');
        break;
    case 2:
        $url .= urlencode('
area["wikipedia"="fr:Saint-Étienne"]->.a;
(
  node
    ["amenity"="charging_station"](area.a);
  way
    ["amenity"="charging_station"](area.a);
  relation
    ["amenity"="charging_station"](area.a);
);
out body;
>;
out skel qt;

        ');
        //$url .= urlencode('area["wikipedia"="fr:Saint-Étienne"]->.a;(node["amenity"="charging_station"](area.a);way["amenity"="charging_station"](area.a);relation["amenity"="charging_station"](area.a););out body;>;out skel qt;');
        break;
}



//$start = microtime(true);

$jsonOsmData = file_get_contents($url);
//var_dump($jsonOsmData);

//$osmData = json_decode($jsonOsmData);
//printf("Query returned %2\$d node(s) and took %1\$.5f seconds.\n\n <br/><br/>", microtime(true) - $start, count($osmData->elements));


//echo "var testjson = ";
echo $jsonOsmData;

?>