$(document).ready(function(){
    $('#submitConnect').on('click',submit);
    $("#deconnexion").on('click',logOut);
    $('#connexion').on('click',refreshForm);
    $('#name').on('click',refreshSpan);
    $('#password').on('click',refreshSpan);
});

function submit(){
    // Requete ajax pour connecté l'utilisateur
    var name = $('#name').val();
    $('#name').prev('label').children('span').html("");
    var password = $('#password').val();
    $('#password').prev('label').children('span').html("");
    $.ajax('connexion.php',{
        method: 'POST',
        data:{name:name,password:password,mode:'logIn'},
        success: connect,
        error:error       
    });   
}

function connect(data){ 
    // Si connection réussi on refresh la page sinon on affiche un message d'erreur dans le formulaire de connexion
    switch (data) {
    case '0':
        $('#name').parent().prev('div').children('span').html("Utilisateur introuvable");
        break;
    case '1':
        $('#password').parent().prev('div').children('span').html("Mot de passe erroné");
        break;
    case '2':
        reload();
        break;
    }
}

function logOut(){
    // Requete ajax pour se deconnecter
    $.ajax('connexion.php',{
        method: 'POST',
        data:{mode:'logOut'},
        success: reload,
        error:error       
    });
}

function refreshForm(){
    // Réinitialisation du formulaire de connexion
    $('#name').parent().prev('div').children('span').html("");
    $('#password').parent().prev('div').children('span').html("");
    $('#name').val('');
    $('#password').val('');
}

function refreshSpan(){
    $(this).parent().prev('div').children('span').html("");
}

function reload(){
    // Recharger la page
    location.reload();
} 

function error(){
    alert('error');
}