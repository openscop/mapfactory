// Initialisation//////////////////////////////////////////////////////////////
$(document).ready(function(){  
    loadMap();
    map.on('click',clickMap);
    $('.deleteTaxo').on('click',confirmDelete);
    $('.modifTaxo').on('click',inputModif);
    $('#addTaxo').on('click',addTaxo);
    $('#listLayer').on('click',layerMode);
    $('#newLayer').on('click',layerMode);
    $('#source').on('change',sourceLayerMode);
    $('.deleteLayer').on('click',confirmDelete);
    $('.modifLayer').on('click',layerModif);
    $('.newLayer').hide();
    $('.osm').hide();  
    $('#addUser').on('click',addUser);   
    $('.deleteUser').on('click',deleteUser);
    $('.seeLayer').on('click',showLayer);
    $('.layerList').append('<div class="layerByTaxo" id="noSelectTaxo"><p>Aucune catégorie sélectionnée</p></div>');
    $('#noSelectTaxo').css('display','block');
    $('#resize').on('click',fullScreen);
    $('#toolHand').on('click',changeTool);
    $('#toolPoint').on('click',changeTool);
    $('#toolLine').on('click',changeTool);
    $('#toolPoly').on('click',changeTool);
    $('#newDraw').hide();
    $('#newDraw').on('click',newDraw);
    $('#seeOsmData').on('click',convertOvsqlToUrl);
});

// Fonctions Taxonomie/////////////////////////////////////////////////////////
function deleteTaxo(){
    // Requete ajax pour suprimer une ligne de la bdd;
    var id = $(this).attr('id');
    $.ajax('delete.php',{
        method: 'POST',
        data: {id:id,mode:'taxo'},
        success: supprTaxo,
        error: error        
    });
}

function inputModif(){
    // Afficher un input pour modifier le nom d'une categorie     
    var explodeId = $(this).attr('id').split('-');
    var name = $('#Taxo-'+explodeId[1]).children('span').html();
    $('#Taxo-'+explodeId[1]).children('span').hide();
    $('#Taxo-'+explodeId[1]).append('<input type="text" value="'+name+'"/> <button id="cancelModif-'+explodeId[1]+'" class="btn btn-danger btn-sm"><i class="fas fa-times"></i></button> <button id="validModif-'+explodeId[1]+'" data-id="'+explodeId[1]+'" class="btn btn-success btn-sm"><i class="fas fa-check"></i></button>');
    $('#validModif-'+explodeId[1]).on('click',modifTaxo);
    $('#cancelModif-'+explodeId[1]).on('click',cancelModif);
    $('.deleteTaxo').prop("disabled",true);
    $('.modifTaxo').prop("disabled",true);
}

function cancelModif(){
    // Enlever l'input de modification
    $(this).prev('input').remove();
    $(this).next('button').remove();
    $(this).prev('span').show();
    $(this).remove();
    $('.deleteTaxo').prop("disabled",false);
    $('.modifTaxo').prop("disabled",false);
}

function modifTaxo(){
    // Requete ajax pour update de la table taxonomy_terms
    var name =$(this).prev('button').prev('input').val();
    var id = $(this).attr('data-id');
    $.ajax('update.php',{
        method: 'POST',
        data:{name:name,id:id,mode:'taxo'},
        success: successModifTaxo,
        error:error
    });
}

function supprTaxo(data){
    // Message de confirmation
    if(data === 'error'){
        error();
    }else{
        $('#confirmDeleteTaxo').remove();
        $('.msg').html('<p>La supréssion à été réalisé avec succès<p>');
        $('#Taxo-'+data).remove();
        $('#taxoTitle-'+data).parent().remove();
        $('#opt-'+data).remove(); 
        $('.deleteTaxo').prop("disabled",false);
        $('.modifTaxo').prop("disabled",false);
    }   
}

function addTaxo(){
    // Verifier que le champ nom est bien rempli, si oui lancer une requete ajax pour ajouter une ligne à la base taxonomy_term
    var name = $('#name').val();
    $.ajax('add.php',{
       method: 'POST',
       data: {name:name,mode:'taxo'},
       success: successAddTaxo,
       error: error
    });    
}

function successAddTaxo(data){
    // Rafraichir la liste taxonomie et le select des taxo data = [liste des categorie, option du formulaire categorie,id taxonomie]
    $(".list").html(data[0]);
    $('.deleteTaxo').on('click',confirmDelete);
    $('.modifTaxo').on('click',inputModif);
    $('.seeLayer').on('click',showLayer);
    $('#selectTaxonomy').html(data[1]);
    var name = $('#name').val();
    $('#name').val('');
    $('.msg').html('');
    $('.layerByTaxo').hide();
    $('.layerList').append('<div class="layerByTaxo" data-idtaxo="'+data[2]+'"><h4 id="taxoTitle-'+data[2]+'">'+name+'</h4></div>');
    $('#taxoTitle-'+data[2]).parent().show();
}

function successModifTaxo(data){
    // Confirmer la modification (data = [success ou error,id dans liste taxo,nom,id de l'option dans le select de taxo,html pour le select taxo,id titre dans la liste des layer])
    if(data[0] === "success"){
        $(data[1]).children('span').html(data[2]);
        $(data[1]).children('span').show();
        for(var i=0;i<3;i++){
            $(data[1]).children(':last-child').remove();
        }
        $(data[3]).remove();
        $('#selectTaxonomy').append(data[4]);
        $('.msg').html("<p>La modification vient d'être réalisé</p>");
        $('.deleteTaxo').prop("disabled",false);
        $('.modifTaxo').prop("disabled",false);
        $(data[5]).html(data[2]);
    }else{
        error();
    }
}

// Fonctions Layer/////////////////////////////////////////////////////////////
function layerMode(){
    // Afficher la liste des layer ou le formulaire de création d'un nouveau layer
    if($(this).attr('id') === 'listLayer'){
        $('.newLayer').hide();
        $('.layerList').show();
        $('.msg').html("");
        //$('.layerByTaxo').show();
    }else{
        $('.newLayer').show();
        $('.layerList').hide();
        $('.osm').hide(); 
        $('.interne').show();
        $('#nameLayer').val('');  
        $('#selectTaxonomy').parent().show();
        $('#source').parent().show();
        $('#source').val('internal');
        $('#color').val('');
        $('#technical_comment').val('');
        $('#technical_comment').val('');
        $('#osm_wiki_link').val('');
        $('#osm_tag').val('');
        $('#overpassql').val('');
        $('#validLayer').unbind();
        $('#cancelLayer').unbind();
        $('#validLayer').on('click',addLayer);
        $('#cancelLayer').on('click',cancel);
        $('#seeOsmData').attr('data-idLayer',0);
        resetMap();
    }
}

function sourceLayerMode(){
    // Pour le formulaire de création ou d'edition d'un layer on change une partie du formulaire en fonction du type de source(internal ou OSM)
    var mode = $(this).val();
    if(mode === 'internal'){
        $('.osm').hide(); 
        $('.interne').show(); 
    }else{
        $('.osm').show(); 
        $('.interne').hide(); 
    }   
}

function deleteLayer(){
    // Requete ajax pour suprimer une ligne de la bdd;
    var id = $(this).attr('id');
    $.ajax('delete.php',{
        method: 'POST',
        data: {id:id,mode:'layer'},
        success: supprLayer,
        error: error        
    });
}

function supprLayer(data){
    // Message de confirmation
    $('.msg').html('<p>La supréssion à été réalisé avec succès<p>');
    $('#layer-'+data).next('div').remove();
    $('#layer-'+data).remove();
}

function layerModif(){
    // Affiche le formulaire de modification d'un layer  
    resetMap();
    $('.newLayer').show();
    $('.layerList').hide();
    var id = $(this).attr("data-id");
    var name = $(this).attr("data-name");
    var overpassql = $(this).attr("data-overpassql");
    var visible_by_default = $(this).attr("data-visible");
    var osm_tag = $(this).attr("data-osm_tag");
    var osm_wiki_link = $(this).attr("data-wiki");
    var technical_comment = $(this).attr("data-comment");
    var color = $(this).attr("data-color");
    var source = $(this).attr("data-source");
    $('#nameLayer').val(name);   
    if(visible_by_default === '1'){
        $('#visibility').attr('checked',true);
    }else{
        $('#visibility').attr('checked',false);
    }
    $('#selectTaxonomy').parent().hide();
    $('#source').parent().hide();
    $('#color').val('#'+color);
    $('#technical_comment').val(technical_comment);
    $('#technical_comment').val(technical_comment);
    $('#osm_wiki_link').val(osm_wiki_link);
    $('#osm_tag').val(osm_tag);
    $('#overpassql').val(overpassql);
    if(source === 'OSM'){
        $('.osm').show(); 
        $('.interne').hide();
        $('#seeOsmData').attr('data-idLayer',id);
    }else{
        $('.osm').hide(); 
        $('.interne').show(); 
        loadGeojson(id);
    }
    $('#validLayer').unbind();
    $('#cancelLayer').unbind();
    $('#validLayer').attr('data-id',id);
    $('#validLayer').attr('data-source',source);
    $('#validLayer').on('click',updateLayer);
    $('#cancelLayer').on('click',cancel);
}

function updateLayer(){
    // Requete ajax pour mettre à jour un layer
    var id = $(this).attr('data-id');
    var name = $('#nameLayer').val();
    var ovsql = $("#overpassql").val();
    var visible_by_default = 0;
    if(document.getElementById('visibility').checked){
        visible_by_default = 1;
    }
    var osm_tag = $("#osm_tag").val();
    var osm_wiki_link = $("#osm_wiki_link").val();
    var technical_comment = $("#technical_comment").val();
    var color = $("#color").val().substr(1);
    var source = $(this).attr('data-source');
    var geojson = convertToGeojson();
    var tag = updateTag();
    $.ajax('update.php',{
        method: 'POST',
        data:{name:name,id:id,ovsql:ovsql,visible_by_default:visible_by_default,osm_tag:osm_tag,osm_wiki_link:osm_wiki_link,technical_comment:technical_comment,color:color,source:source,geojson:geojson,mode:'layer',tag:tag},
        success: successModifLayer,
        error:error
    });
}
function successModifLayer(data){
    // Mettre à jour la liste des layer après l'update (data = [id du layer dans la liste,nouvelle donnée html pour le layer])
    $('.msg').html('<p>Modification éffectué</p>');
    var categorie = $(data[0]).parent('div');
    $('.layerByTaxo').hide();
    $(data[0]).remove();
    categorie.append(data[1]);
    $('.newLayer').hide();
    $('.layerList').show();
    $(categorie).show();
    categorie.children('p:last-child').children('button:first-child').on('click',confirmDelete);
    categorie.children('p:last-child').children('.modifLayer').on('click',layerModif);
}

function addLayer(){
    // Requete ajax pour créer un nouveau layer
    var name = $('#nameLayer').val();
    var ovsql = $("#overpassql").val();
    var visible_by_default = 0;
    if(document.getElementById('visibility').checked){
        visible_by_default = 1;
    }
    var osm_tag = $("#osm_tag").val();
    var osm_wiki_link = $("#osm_wiki_link").val();
    var technical_comment = $("#technical_comment").val();
    var color = $("#color").val().substr(1);
    var source = $('#source').val();
    var idTaxonomy = $('#selectTaxonomy').val();  
    var geojson = convertToGeojson();
    var tag = updateTag();
    $.ajax('add.php',{
        method: 'POST',
        data:{name:name,ovsql:ovsql,visible_by_default:visible_by_default,osm_tag:osm_tag,osm_wiki_link:osm_wiki_link,technical_comment:technical_comment,color:color,source:source,idTaxonomy:idTaxonomy,geojson:geojson,mode:'layer',tag:tag},
        success: successAddLayer,
        error: error
    });   
}

function successAddLayer(data){
        // Réactualiser la fiche la liste des layer [html layerByTaxo, id Taxo]
        $('.msg').html('');
        $('.layerList').html(data[0]);
        $('.deleteLayer').on('click',confirmDelete);
        $('.modifLayer').on('click',layerModif); 
        $('.newLayer').hide();
        $('.layerList').show();
        $('.layerByTaxo').hide();
        $('[data-idtaxo='+data[1]+']').show();
        $('.msg').html('<p>Le nouveau layer à bien été enregistré</p>');
}

function showLayer(){
    // Afficher les layer d'une categorie
    var id = $(this).attr('id');
    $('.layerByTaxo').hide();
    $('[data-idtaxo='+id+']').show();
    $('.newLayer').hide();
    $('.layerList').show(); 
    $('.msg').html('');
    $('#opt-'+id).prop('selected',true);
}

// Fonctions d'erreur,d'annulation et confirmation//////////////////////////////////////////
function error(){
    // Affiche un message en cas d'erreur d'une requete ajax
    $('.msg').html("<p>Une erreur est survenue<p>");
    //hideUserForm();
}

function cancel(){
    // Efface le message et retour à la liste layer
    $('.msg').html("");
    $('.newLayer').hide();
    $('.layerList').show();
}

function confirmDelete(){
    // Affichage message de confirmation de supression   
    if($(this).hasClass('deleteTaxo')){
        $('.deleteTaxo').prop("disabled",true);
        $('.modifTaxo').prop("disabled",true);
        var name = $(this).attr('data-name');
        var explodeId = $(this).attr('id').split('-');
        var id = explodeId[1];
        $(this).parent('div').after('<div id="confirmDeleteTaxo"><span>Suprimer la catégorie '+name+': </span><button class="valid btn btn-success btn-sm" id="'+id+'"><i class="fas fa-check"></i></button> <button class="cancel btn btn-danger btn-sm"><i class="fas fa-times"></i></button></div>');
        $(this).parent().hide();
        $('.valid').on('click',deleteTaxo);  
        $('.cancel').on('click',cancelDeleteTaxo);
    }else if($(this).hasClass('deleteLayer')){
        var name = $(this).attr('data-name');
        var id = $(this).attr('data-id');
        $(this).parent('p').after('<div><span>Suprimer le layer '+name+': </span><button class="valid btn btn-success btn-sm" id="'+id+'"><i class="fas fa-check"></i></button> <button class="cancel btn btn-danger btn-sm"><i class="fas fa-times"></i></button></div>');
        $(this).parent('p').hide();
        $('.valid').on('click',deleteLayer);
        $('.cancel').on('click',cancelDeleteLayer);
    }
}

function cancelDeleteTaxo(){
    // Effacer le message de confirmation de supression
    $(this).parent().prev('div').show();
    $(this).parent().remove(); 
    $('.deleteTaxo').prop("disabled",false);
    $('.modifTaxo').prop("disabled",false);
}

function cancelDeleteLayer(){
    // Effacer le message de confirmation de supression
    $(this).parent().prev('p').show();
    $(this).parent().remove();
}

// Map ////////////////////////////////////////////////////////////////////////
//var map = L.map('mapid').setView([45.4333, 4.4], 13);
var map = L.map('mapid',{
    center:[45.4333, 4.4],
    zoom:13
});

function loadMap(){
    // Chargement de la carte
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWFwZmFjdG9yeSIsImEiOiJjaWV0dnIzZXIwMDRkc3prczVxZDB1OTM5In0.JmReSQrH-4l_IJDxcs_8TQ'
    }).addTo(map);  
}

var tool = 'toolHand';
function changeTool(){
    // Changement d'outil pour dessiner sur la carte
    if(tool === 'toolLine' || tool==='toolPoly'){
        if(marker[indexFeature]){
            indexFeature++;          
        }else if(feature[indexFeature]){
            feature.splice(indexFeature,1);
        }  
        $('#newDraw').hide();
    }
    $('#'+tool).removeClass('btn-info');
    $('#'+tool).addClass('btn-dark');
    tool = $(this).attr('id');
    $('#'+tool).removeClass('btn-dark');
    $('#'+tool).addClass('btn-info');
}

var feature = [];
var indexFeature = 0;
var marker = [];
function clickMap(evt){
    // Dessin sur la carte en fonction de l'outil selectionné
    if(tool === 'toolPoint'){
        feature[indexFeature] = {"type":"Feature","properties":{"tags":{"name":""}},"geometry":{"type":"Point","coordinates":[evt.latlng.lng,evt.latlng.lat]}};
        marker[indexFeature] = L.geoJson(feature[indexFeature]).addTo(map);
        marker[indexFeature].bindPopup('<input type="text" onchange="markerName('+indexFeature+')" id="marker-'+indexFeature+'"/> <button class="btn btn-danger btn-sm" onclick="markerDelete('+indexFeature+')"><i class="fas fa-times"></i> Supprimer</button>');        
        indexFeature++;
    }else if(tool === 'toolLine'){
        if(feature[indexFeature]){          
            feature[indexFeature].geometry.coordinates.push([evt.latlng.lng,evt.latlng.lat]);
            if(marker[indexFeature]){
                map.removeLayer(marker[indexFeature]);
            }
            marker[indexFeature] = L.geoJson(feature[indexFeature]).addTo(map);
            var name = feature[indexFeature].properties.tags.name;
            marker[indexFeature].bindPopup('<input type="text" onchange="markerName('+indexFeature+')" value="'+name+'" id="marker-'+indexFeature+'"/> <button class="btn btn-danger btn-sm" onclick="markerDelete('+indexFeature+')"><i class="fas fa-times"></i> Supprimer</button>');
            $('#newDraw').show();
        }else{
            feature[indexFeature] = {"type":"Feature","properties":{"tags":{"name":""}},"geometry":{"type":"LineString","coordinates":[[evt.latlng.lng,evt.latlng.lat]]}};
        }
    }else if(tool === 'toolPoly'){
        if(feature[indexFeature]){
            feature[indexFeature].geometry.coordinates[0].push([evt.latlng.lng,evt.latlng.lat]);
            if(marker[indexFeature]){
                map.removeLayer(marker[indexFeature]);
            }
            marker[indexFeature] = L.geoJson(feature[indexFeature]).addTo(map);
            var name = feature[indexFeature].properties.tags.name;
            marker[indexFeature].bindPopup('<input type="text" onchange="markerName('+indexFeature+')" value="'+name+'" id="marker-'+indexFeature+'"/> <button class="btn btn-danger btn-sm" onclick="markerDelete('+indexFeature+')"><i class="fas fa-times"></i> Supprimer</button>');
            $('#newDraw').show();
        }else{
            feature[indexFeature] = {"type":"Feature","properties":{"tags":{"name":""}},"geometry":{"type":"Polygon","coordinates":[[[evt.latlng.lng,evt.latlng.lat]]]}};
        }
    }
}

function markerName(index){
    // Modifier le popup d'un marker
    var name = $("#marker-"+index).val();
    marker[index].bindPopup('<input type="text" onchange="markerName('+index+')" value="'+name+'" id="marker-'+index+'"/> <button class="btn btn-danger btn-sm" onclick="markerDelete('+index+')"><i class="fas fa-times"></i> Supprimer</button>');
    feature[index].properties.tags.name = name;
}

function markerDelete(index){
    // Suprimer un marker
    feature[index] = null;
    map.removeLayer(marker[index]);
    marker[index] = null;
}

function newDraw(){
    // Commencer une nouvelle feature
    indexFeature++;
    $('#newDraw').hide();
}

function convertToGeojson(){
    // Convertir les données de la carte en un seul geojson puis le convertir en chaine de caractère l'envoi en ajax
    var geojson = {"type":"FeatureCollection","features":[]};
    feature.forEach(function(feat){
        if(feat !== null){
            geojson.features.push(feat);
        }
    });
    return JSON.stringify(geojson);
}

function resetMap(){
    // Effacer les markeurs de la map et remettre les tableau de geojson a 0    
    marker.forEach(function(elt){
        if(elt !== null){
            map.removeLayer(elt);
        }
    });
    map.closePopup();
    marker = [];
    feature = [];
    indexFeature = 0;
    tableTag = [];
    tableKey = [];
    tableKeyExemple = [];
    $('#tagsPannel').html("");
    $('#toolBar').show();
    if(tool !== 'toolHand'){
        $('#'+tool).removeClass('btn-info');
        $('#toolHand').removeClass('btn-dark');
        $('#'+tool).addClass('btn-dark');
        $('#toolHand').addClass('btn-info');
    }   
    tool = 'toolHand';
}

function loadGeojson(id_layer){
    // Requete ajax pour charger les données geojson d'un layer
    $.ajax('loadGeojson.php',{
        method: 'POST',
        data:{id_layer:id_layer},
        success: drawGeojson
    });
}

function drawGeojson(data){
    // Dessiner les données d'un layer sur la carte
    var geojson = JSON.parse(data);
    geojson.features.forEach(function(feat){
        feature[indexFeature] = feat;
        marker[indexFeature] = L.geoJson(feature[indexFeature]).addTo(map);
        var name = feat.properties.tags.name;
        marker[indexFeature].bindPopup('<input type="text" onchange="markerName('+indexFeature+')" value="'+name+'" id="marker-'+indexFeature+'"/> <button class="btn btn-danger btn-sm" onclick="markerDelete('+indexFeature+')"><i class="fas fa-times"></i> Supprimer</button>');
        indexFeature++;
    });
    indexFeature++;
}

var scroll = 0;
function fullScreen(){
    // Afficher la map en plein ecran
    scroll = window.scrollY;
    $('.interne').addClass('fullScreen');
    $('#divMap').css('height',window.innerHeight-150);
    $('#resize').unbind();
    $('#resize').on('click',smallMap);
    $('#mapid').css('height','110%');
    map.invalidateSize();
    $(window).on('resize',resize);
}

function smallMap(){
    // Reduire la carte
    $('.interne').removeClass('fullScreen');
    $('#mapid').css('height','400px');
    $('#divMap').css('height','400px');
    window.scrollTo(0,scroll);
    $('#resize').unbind();
    $('#resize').on('click',fullScreen);
    map.invalidateSize();
    $(window).unbind();
}

function resize(){
    $('#divMap').css('height',window.innerHeight-60);
}

//Fonctions pour la map quand les données utiliséproviennet d'OSM//////////////
function convertOvsqlToUrl(){
    // Requete pour obtenir une url pour interroger l'api overpass et recevoir les donnée des tag modifier
    var ovsql = $('#overpassql').val();
    var idLayer = $(this).attr('data-idLayer');
    $.ajax('convertOvsqlToUrl.php',{
        method: 'POST',
        data:{ovsql:ovsql,idLayer:idLayer},
        success:overpassApi
    });
}

var dataOverpass;
function overpassApi(data){
    // Utilisation d'une url pour recevoir des données de l'api overpass
    dataOverpass = JSON.parse(data);
    $.ajax(dataOverpass.url,{
        success:convertGeojson
    });
}

var tableTag = [];
var tableTagCustom = [];
var tableKey = [];
var tableKeyExemple = [];
function convertGeojson(data){
    // Convertir les données de l'api overpass en geojson puis les afficher sur la carte
    var geojson = osmtogeojson(data);
    tableTag = [];
    tableTagCustom = [];   
    $('.interne').show();
    $('#toolBar').hide();
    geojson.features.forEach(function(feat){
        marker[indexFeature] = L.geoJson(feat).addTo(map);
        var isCustom = false;
        for(var idFeat in dataOverpass.tags) { 
                if(idFeat === feat.id){
                    isCustom = true;                                     
                }
            }
        if(isCustom){
            tableTag[feat.id] = {"customized":true,"tags":{}};
        }else{
            tableTag[feat.id] = {"customized":false,"tags":{}};
        }
        var keys = Object.entries(feat.properties.tags);
        var html = "";
        var pos = 0;
        var stringFeat = '"'+feat.id+'"';
        keys.forEach(function(tag){
            if(!tableKey.includes(tag[0])){
                tableKey.push(tag[0]);
            }
            if(!tableKeyExemple[tag[0]]){
                tableKeyExemple[tag[0]] = [];
            }
            if(!tableKeyExemple[tag[0]].includes(tag[1])){
                tableKeyExemple[tag[0]].push(tag[1]);
            }           
            if(tableTag[feat.id].customized){                
                tableTag[feat.id].tags[tag[0]] = {"show":dataOverpass.tags[feat.id][tag[0]].show,"pos":dataOverpass.tags[feat.id][tag[0]].pos,"title":tag[1]};
                if(!tableTagCustom[feat.id]){
                    tableTagCustom[feat.id] = [];
                    
                }
                tableTagCustom[feat.id][dataOverpass.tags[feat.id][tag[0]].pos] = {"show":dataOverpass.tags[feat.id][tag[0]].show,"pos":dataOverpass.tags[feat.id][tag[0]].pos,"title":tag[1],"index":indexFeature,"key":tag[0]};

            }else{
            tableTag[feat.id].tags[tag[0]] = {"show":false,"pos":pos,"title":tag[1]};
            var stringTag = '"'+tag[0]+'"';
            html += "<div><div class='btn-group-vertical btn-group-sm d-flex justify-content-center'><button class='btn' onclick='upTag("+stringTag+","+stringFeat+","+indexFeature+")'><i class='fas fa-arrow-up'></i></button><div><input type='checkbox'  onclick='showTag("+stringTag+","+stringFeat+",this,"+indexFeature+")'/><span><b> "+tag[0]+" :</b> "+tag[1]+" </span></div><button class='btn' onclick='downTag("+stringTag+","+stringFeat+","+indexFeature+")'><i class='fas fa-arrow-down'></i></button></div></div><br>"; 
            pos++;
        }});
        marker[indexFeature].bindPopup(html,{maxWidth:"1000"});
        indexFeature++;
    });
   for(var feat in tableTagCustom){
       var html = "";
       tableTagCustom[feat].forEach(function(tag){
           var stringTag = '"'+tag.key+'"';
           var stringFeat = '"'+feat+'"';
           var index = tag.index;
           var tag0 = tag.key;
           var tag1 = tag.title;
           html += "<div><div class='btn-group-vertical btn-group-sm d-flex justify-content-center'><button class='btn' onclick='upTag("+stringTag+","+stringFeat+","+index+")'><i class='fas fa-arrow-up'></i></button><div><input type='checkbox'  onclick='showTag("+stringTag+","+stringFeat+",this,"+index+")'";
           if(tag.show){
               html += " checked";
           } 
           html += "/><span><b> "+tag0+" :</b> "+tag1+" </span></div><button class='btn' onclick='downTag("+stringTag+","+stringFeat+","+index+")'><i class='fas fa-arrow-down'></i></button></div></div><br>";
           marker[index].bindPopup(html,{maxWidth:"1000"});
       });
   }
   mainTag();
}

function mainTag(){
    // Créer une liste de tous les tags d'OSM
    $('#tagsPannel').html("<h3>Tags</h3>");
    tableKey.forEach(function(tag,index){
        $('#tagsPannel').append('<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><input id="tag-'+index+'" type="checkbox"></div></div><input type="text" class="form-control" value="'+tag+'" disabled="disabled"><div class="input-group-append"><input type="number" id="pos-'+index+'" min="1" class="input-group-text"><button class="btn btn-primary showExemple">Voir</button></div></div>');
        var i = 0;
        var list = '<div class="listExemple"><ul>';
        tableKeyExemple[tag].forEach(function(exemple){
            if(i<50){
                list += '<li>'+exemple+'</li>';
            }
            i++;
        });        
        list += '</ul><div>';                    
        $('#tagsPannel').append(list);      
    });
    $('#tagsPannel').append("<button onclick='allTags()' class='btn btn-success float-right'>Appliquer à tout les éléments</button>");
    $('.showExemple').on('click',showExemple);
}

function allTags(){
    // Enregistrer une modification des préférences d'affichage des tags d'osm
    var orderTag = [];
    tableKey.forEach(function(tag,index){
        if($('#pos-'+index).val() === ""){
            if(!orderTag[0]){
                orderTag[0] = [];
            }
            orderTag[0].push(tag);
        }else{
            if(!orderTag[$('#pos-'+index).val()]){
                orderTag[$('#pos-'+index).val()] = [];
            }
            orderTag[$('#pos-'+index).val()].push(tag);
        }
        
    });
    for(var feat in tableTag){
        var pos = 0;
        tableTag[feat].customized = true;
        orderTag.forEach(function(tags,index){
            tableKey.forEach(function(tag,index){
                if(tableTag[feat].tags[tag]){
                    tableTag[feat].tags[tag].show = $('#tag-'+index).prop('checked');
                }
            });
            if(index > 0){
                tags.forEach(function(tag){              
                    if(tableTag[feat].tags[tag]){
                       // tableTag[feat].tags[tag].show = $('#tag-'+index).prop('checked');
                        tableTag[feat].tags[tag].pos = pos;
                        pos++;
                    }
                });
            }
        });
        orderTag[0].forEach(function(tag){
            if(tableTag[feat].tags[tag]){
                //tableTag[feat].tags[tag].show = $('#tag-'+index).prop('checked');
                tableTag[feat].tags[tag].pos = pos;
                pos++;
            }
        });
        
    }
    $('#validLayer').click();
}

function showExemple(){
    // Afficher une liste d'example d'une categorie de tag
    if($(this).html() === 'Voir'){
        $(this).html("Cacher");
        $(this).parent().parent().next('div').show();
    }else{
        $(this).html("Voir");
        $(this).parent().parent().next('div').hide();
    }
}

function showTag(tag,feat,checkBox,index){
    // Enregistrement dans le tableau tableTag d'une modification de l'affichage
    tableTag[feat].tags[tag].show = checkBox.checked;
    tableTag[feat].customized = true;
    reBind(index,feat);
}

function downTag(tag,feat,index){
    // Déplacer un tag vers le bas
    var pos = tableTag[feat].tags[tag].pos;
    pos++;
    var move = false;
    for(let [key,val] of Object.entries(tableTag[feat].tags)){
        if(val.pos === pos){
            move = true;
            tableTag[feat].tags[key].pos--;
            tableTag[feat].tags[tag].pos++;
            tableTag[feat].customized = true;
        }
    }
    if(move){
        reBind(index,feat);
        marker[index].openPopup();
    }
}

function upTag(tag,feat,index){
    // Déplacer un tag vers le haut
    var pos = tableTag[feat].tags[tag].pos;
    pos--;
    var move = false;
    for(let [key,val] of Object.entries(tableTag[feat].tags)){
        if(val.pos === pos){
            move = true;
            tableTag[feat].tags[key].pos++;
            tableTag[feat].tags[tag].pos--;
            tableTag[feat].customized = true;
        }
    }
    if(move){
        reBind(index,feat);
        marker[index].openPopup();
    }
}

function reBind(index,feat){
    // Réactualiser le popup d'un markeur
    var html = "";
    var orderTag = [];
    for (let [key,val] of Object.entries(tableTag[feat].tags)) {
      orderTag[val.pos] = [key,val];
    }
    var stringFeat = '"'+feat+'"';
    orderTag.forEach(function(tab){
        //console.log(tab);
        var stringTag = '"'+tab[0]+'"';
        if(tab[1].show){           
            html += "<div><div class='btn-group-vertical btn-group-sm d-flex justify-content-center'><button class='btn' onclick='upTag("+stringTag+","+stringFeat+","+index+")'><i class='fas fa-arrow-up'></i></button><div><input type='checkbox'  onclick='showTag("+stringTag+","+stringFeat+",this,"+index+")' checked /><span><b> "+tab[0]+" :</b> "+tab[1].title+" </span></div><button class='btn' onclick='downTag("+stringTag+","+stringFeat+","+index+")'><i class='fas fa-arrow-down'></i></button></div></div><br>";
        }else{
            html += "<div><div class='btn-group-vertical btn-group-sm d-flex justify-content-center'><button class='btn' onclick='upTag("+stringTag+","+stringFeat+","+index+")'><i class='fas fa-arrow-up'></i></button><div><input type='checkbox'  onclick='showTag("+stringTag+","+stringFeat+",this,"+index+")'/><span><b> "+tab[0]+" :</b> "+tab[1].title+" </span></div><button class='btn' onclick='downTag("+stringTag+","+stringFeat+","+index+")'><i class='fas fa-arrow-down'></i></button></div></div><br>";
        }
    });
    marker[index].bindPopup(html,{maxWidth:"1000"});
}

function updateTag(){
    // Trier les tag qui ont été modifier
    var tagToUpdate = {};
    var i = 0;
    for(var feat in tableTag){
        if(tableTag[feat].customized){
            update = true;
            tagToUpdate[i] = {};
            tagToUpdate[i]["id_feature"] = feat;
            tagToUpdate[i]["tag"] = {};
            for(var keyTag in tableTag[feat].tags){
                tagToUpdate[i]["tag"][keyTag]= {};
                tagToUpdate[i]["tag"][keyTag]["pos"] = tableTag[feat].tags[keyTag].pos;
                tagToUpdate[i]["tag"][keyTag]["show"] = tableTag[feat].tags[keyTag].show;
            }
            i++;
        }
    }
    return tagToUpdate;
}

//Fonctions utilisateur////////////////////////////////////////////////////////
function addUser(){
    // Requete ajax pour ajouter un nouvel utilisateur
    var name = $('#userName').val();
    var password = $('#password').val();
    var password2 = $('#password2').val();
    if(name === ""){
        $('#msgFormUser').html("Le champ Nom n'est pas renseigné");
    }else if(password === ""){
        $('#msgFormUser').html("Le champ Password n'est pas renseigné");
    }else if(password !== password2){
        $('#msgFormUser').html("Les mots de passe ne sont pas identiques");
    }else{
        $.ajax('add.php',{
            method: 'POST',
            data:{name:name,password:password,mode:'user'},
            success: confirmAddUser
        });
    }
}

function confirmAddUser(data){
    // Confirmer l'ajout d'un nouvel utilisateur
    switch(data['res']){
        case 0:
            $('#msgFormUser').html("Le nom d'utilisateur est déja utilisé");
            break;
        case 1:
            $('#msgFormUser').html("Une erreur est survenu lors de l'enregistrement");
            break;
        case 2:
            $('#msgFormUser').html("");
            $('#list').append("<div id='user-"+data['id']+"' class='eltListUser'><button class='deleteUser btn btn-danger btn-sm'  id='"+data['id']+"'><i class='fas fa-trash-alt'></i></button><span> "+data['name']+"</span></div>");
            $('#user-'+data['id']).children('button').on('click',deleteUser);
            $('#userName').val('');
            $('#password').val('');
            $('#password2').val('');
            break;                    
    }
}    

function deleteUser(){
    // Requete ajax pour suprimer un utilisateur
    var id = $(this).attr('id');
    $.ajax('delete.php',{
        method: 'POST',
        data: {id:id,mode:'user'},
        success: eraseUser
    });
}

function eraseUser(data){
    // Effacer un utilisateur de la liste
    if(data !== 'error'){
        $('#user-'+data).remove();
    }
}