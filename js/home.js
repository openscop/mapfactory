/**
 * Created by David on 04/06/2015.
 */

var allLayers = new Array();
var tableTag;
$(document).ready(function () {
/////////////////////////////////////////////
    // Chargement des tags pour les popup
    $.ajax('loadTag.php',{
        success:function(data){
            tableTag = JSON.parse(data);
        }
    });






           /////////////////////////////////////

//BOF cache activation button

    function option_selected() {
        $.ajax({
            dataType: "json",
            url: "?w=cache&get_state=1", //server call
            success: function (data) {
                // console.log('data : success');
                // console.log(data.cache);
                if (data.cache == "true")
                    $('#on').addClass('active');
                else
                    $('#off').addClass('active');
            }
        });
    }

    option_selected();

    $('#on').click(function () {
        $('#off').removeClass('active');
        $('#on').addClass('active');
        $.ajax({
            dataType: "json",
            url: "?w=cache&set_state=true", //server call
            success: function (data) {
                // console.log(data);
            }
        });
    });

    $('#off').click(function () {
        $('#on').removeClass('active');
        $('#off').addClass('active');
        $.ajax({
            dataType: "json",
            url: "?w=cache&set_state=false", //server call
            success: function (data) {
                // console.log(data);
            }
        });
    });
//EOF cache activation button


// marker popup
    function onEachFeature(feature, layer) {
        if (feature.hasOwnProperty('properties') && feature.properties.hasOwnProperty('tags')) {
            var popupContent = "<p>" + (feature.properties.tags.name ? feature.properties.tags.name : "Nom inconnu") + "</p>";
            popupContent += "<p><a href='http://www.openstreetmap.org/" + feature.id + "' target='_blank'>Lien OpenStreeMap</a></a></p>";

            popupContent += feature.properties.hasOwnProperty('popupContent') ? feature.properties.popupContent : "";
        }
        layer.bindPopup(popupContent);
    }

//progress bar
    function advanceProgressBar(value, total) {
        var progress = Math.round(value * 100 / total);
        $('.progress-bar').css('width', progress + '%').attr('aria-valuenow', progress).html(progress + '%');
        if (value == total) {
            $('#info-box').hide();
        }
    }

// add markers
    function fillShowLayer(idLayer, GeoJsonData) {
        var dataGeoJson4Leaflet = null;
        var color = $("a[data-id=" + idLayer + "]").siblings("div.color_picker").css("background-color");

        if (GeoJsonData.generator && GeoJsonData.generator.substring(0, 12) == "Overpass API") // OSM
        {
            dataGeoJson4Leaflet = osmtogeojson(GeoJsonData); // convert OSM data to geoJson
        } else if (GeoJsonData.hasOwnProperty("type")) // internal
        {
            dataGeoJson4Leaflet = GeoJsonData;
        }

        if (dataGeoJson4Leaflet) {
            allLayers[idLayer]['GeoJsonLayer'] = L.geoJson([dataGeoJson4Leaflet], {
                //onEachFeature: onEachFeature, // demander explication
                onEachFeature: function(feature,layer){
                    var node = layer.feature.id;
                    var popupContent = "";
                    if(tableTag.tags.hasOwnProperty(node)){
                        console.log(tableTag.tags[node]);
                        var tri = [];
                        $.each(tableTag.tags[node], function(key, value) {
                            tri[value.pos] = [key,value.show];
                        });
                        console.log(tri);
                        tri.forEach(function(value){
                            if(value[1] === true){
                                popupContent += "<p>" +feature.properties.tags[value[0]]+ "</p>";
                            }
                        });
                    }else{
                        if (feature.hasOwnProperty('properties') && feature.properties.hasOwnProperty('tags')) {
                        popupContent += "<p>" + (feature.properties.tags.name ? feature.properties.tags.name : "Nom inconnu") + "</p>";
                    }

                }
                popupContent += "<p><a href='http://www.openstreetmap.org/" + feature.id + "' target='_blank'>Lien OpenStreeMap</a></a></p>";
                layer.bindPopup(popupContent);


                },
                pointToLayer: function (feature, latlng) {
                    L.DivIcon.SVGIcon.custom_marker = L.DivIcon.SVGIcon.extend({ // options : color of the icon
                        options: {
                            "color": color
                        }
                    });
                    L.divIcon.svgIcon.custom_marker = function (options) {
                        return new L.DivIcon.SVGIcon.custom_marker(options);
                    };
                    allLayers[idLayer]['marker'] = new L.Marker(latlng, {icon: new L.DivIcon.SVGIcon.custom_marker()});
                    return allLayers[idLayer]['marker'];
                },
                style: {
                    "color": color,
                    "weight": 5
                }
            });
            allLayers[idLayer]['GeoJsonLayer'].addTo(map);
            nbLayerDone++
            advanceProgressBar(nbLayerDone, nbVisibleLayers);

        } else {
            console.log("Data not supported");
            //Todo : display an error to the user
        }
    }

//get geojson
    function getGeoJson(idLayer) {
        $("a[data-id=" + idLayer + "]").siblings("i.state").removeClass("fa-plus").addClass("fa-spinner fa-spin");
        if (allLayers[idLayer]['GeoJsonData'] == false) {
            $.ajax({
                dataType: "json",
                url: "?w=layer&idl=" + idLayer, //server call
                success: function (data) {
                    allLayers[idLayer]['GeoJsonData'] = data;
                    $("a[data-id=" + idLayer + "]").siblings("i.state").removeClass("fa-spinner fa-spin").addClass("fa-eye");
                    fillShowLayer(idLayer, data);
                },
                timeout: 70000,
                error: function (xhr, status) {
                    allLayers[idLayer]['GeoJsonData'] = false;
                    allLayers[idLayer]['visible'] = false;
                    $("a[data-id=" + idLayer + "]").siblings("i.state").removeClass("fa-spinner fa-spin").addClass("fa-exclamation-triangle");
                    console.log(xhr)
                    if (status == "timeout") {
                        // TODO: Timeout error
                    }
                }
            });
        } else {
            fillShowLayer(idLayer, allLayers[idLayer]['GeoJsonData']);
            $("a[data-id=" + idLayer + "]").siblings("i.state").removeClass("fa-spinner fa-spin").addClass("fa-eye");
        }
    }

    function toggleLayer(linkMenu) {
        id = $(linkMenu).data("id");
        if (allLayers[id]['visible'] && allLayers[id]['GeoJsonData'] != false && allLayers[id]['GeoJsonLayer'] != false) { // the layer is visible
            allLayers[id]['GeoJsonLayer'].clearLayers();
            allLayers[id]['visible'] = false;
            $(linkMenu).siblings("i.state").removeClass("fa-eye fa-exclamation-triangle").addClass("fa-plus");
        } else { // the layer is not visible
            getGeoJson(id);
            allLayers[id]['visible'] = true;
        }
        updatePermalink();
    }

    option_selected();


    var allIdLayers = Object.getOwnPropertyNames(var_srv.configOfLayers);
    var nbLayer = allIdLayers.length;
    var nbLayerDone = 0;
    var visibleLayers = new Array();
    // console.log(var_srv);
    // console.log('allLayers '+allLayers);


    // specific layers are requested in the url ?
    var url = new URL(window.location.href);
    var idLayersRequested = url.searchParams.get("l");
    if (idLayersRequested && idLayersRequested != "none") {
        try {
            idLayersRequested = atob(idLayersRequested);
            idLayersRequested = idLayersRequested.split('-');
        } catch(e) {
            document.location.assign(window.location.protocol + "//" + window.location.hostname + "/");
        }
    }





    var map = L.map('map').setView([45.4333, 4.4], 13);
    var markers = L.markerClusterGroup();

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
        tileSize: 512,
        maxZoom: 18,
        zoomOffset: -1,
        id: 'mapbox/streets-v11',
        accessToken: 'pk.eyJ1IjoibWFwZmFjdG9yeSIsImEiOiJjaWV0dnIzZXIwMDRkc3prczVxZDB1OTM5In0.JmReSQrH-4l_IJDxcs_8TQ'
    }).addTo(map);

    if (idLayersRequested != "none") {
        $('#info-box').html("<i class='fa fa-spinner fa-2x fa-spin'></i><p>Nous interrogeons OpenStreetMap et préparons vos données...</p><p>Cela peut prendre un peu de temps !</p>" +
            "<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%;'>0%</div></div>");
        $('#info-box').show();
    } else {
        $('#info-box').hide();
    }



    // iterate the layers
    $(allIdLayers).each(function (index, value) {
        allLayers[value] = new Array();
        allLayers[value]['GeoJsonData'] = false;
        allLayers[value]['GeoJsonLayer'] = false;

        if (idLayersRequested) {    // specific layers are requested
            if (idLayersRequested.indexOf(value) > -1) {     // is this layer requested in the url ?
                visibleLayers.push(this);
                allLayers[value]['visible'] = true;
            } else {
                allLayers[value]['visible'] = false;
            }
        } else {                    // no specific layer are requested
            if (var_srv.configOfLayers[value].visible_by_default == "1") {  // visible layers by default from server ?
                visibleLayers.push(this);
                allLayers[value]['visible'] = true;
            } else {
                allLayers[value]['visible'] = false;
            }
        }
    });


    // Download GeoJSON data and display it only for the visible layers by default.
    var nbVisibleLayers = visibleLayers.length;
    $(visibleLayers).each(function (index, value) {
        getGeoJson(value);
    });

    map.addLayer(markers);


    // enable menu action
    $('.mf-layer-toggle').click(function () {
        toggleLayer(this);
    });

    $('[data-toggle="tooltip"]').tooltip();


    // permalink

    function updatePermalink() {
        var link = "";
        allLayers.forEach(function(layer, index) {
            if (layer['visible'] == true) {
                if (link != "") link += "-";
                link += index;
            }
        });
        link = (link == "") ? "none" : btoa(link);
        link = window.location.protocol + "//" + window.location.hostname + "/?l=" + link;
        $("#permalink input").val(link);
    }

    $('#permalink').click(function () {
        updatePermalink();
        $("#permalink .hide").removeClass("hide");
    });
    $("#permalink input").click(function () {
        this.select();
        document.execCommand("copy");
    });

});
