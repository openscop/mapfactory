<?php
$page = new Page(_('Home'));
$content['freebox1_content'] = (!is_null($instance->freebox1_content)) ? $instance->freebox1_content : null;

// BOF send the configuration of the layers to the client with Javascript Header
$allLayers = $instance->getAllLayers();
// keep only the necessary values
$configOfLayers = array();
foreach ($allLayers as $id => $oneLayerArray) {
    foreach ($oneLayerArray as $name => $value) {
        if($name=="name" || $name=="visible_by_default" || $name=="osm_tag" || $name=="osm_wiki_link" || $name=="color") {  // add here the variable name to be transmitted
            $configOfLayers[$id][$name] = trimAnywhere($value);
        }
    }
}
$page->addJsVariable('configOfLayers',json_encode($configOfLayers));
// send to the view
$content['layers'] = $configOfLayers;
// EOF send the configuration of the layers to the client

// BOF Search all categories in taxonomy with list of layers for every category
$taxo = new Taxonomy();
$allCat = $taxo->getLevel0();
foreach ($allCat as $index => $cat) {
    $allCat[$index]['layers'] = $taxo->foundRelation($cat['id'],"layer");
}
$content['categories'] = $allCat;
// EOF Search all categories in taxonomy with list of layers for every category

$page->addContentBodyByView('home', $content);

if (!is_null($instance->custom_css)) {
    $page->addCssCode($instance->custom_css);
}

$page->about = $instance->about ;

echo $page->render();
