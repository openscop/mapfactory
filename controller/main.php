<?php
// Load the instance
$instance = new Instance();
define('INSTANCE_NAME', $instance->name);

// Launch controller that corresponds to the work required
$work = isset($_GET['w']) ? strtolower($_GET['w']) : 'home';
if (file_exists(PATH_ROOT . "/controller/$work.php")) {
    require_once(PATH_ROOT . "/controller/$work.php");
} else {
    require_once(PATH_ROOT . "/controller/home.php");
}


