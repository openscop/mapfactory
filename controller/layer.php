<?php
/**
 * Created by David
 * Date: 02/06/2015
 * Time: 22:27
 */

session_start();

require_once(PATH_ROOT."/model/Layer.class.php");

$idl = isset($_GET['idl']) ? strtolower($_GET['idl']) : null;


if ($idl)
{
	$layer = new Layer($idl);
	$jsonFile = new JsonFile();
	$jsonFile->addContent($layer->get_data());
	$jsonFile->render();
}
