# In progress
- in database instance, move the configuration version number to global database
- improve the integration of the color layers in the menu
- check the validity of the nightly updates 

# To do
- create permalinks
    - having the desired layer
    - for a specific marker (with zoom level)
    - to embed a map in a web page
- detailed description of the markers
- integrate the live display of Véliverts availability
  http://www.velivert.fr/vcstations.xml
  http://velivert.smoove.fr/


# Done
v0.6.0 - 09/10/2016
- consideration of layer data that are not originated from OpenStreetMap
- accelerating layer display with setting daily cache OpenStreetMap data
- choice of color layers
- remove the folder "instances" of the GIT repository