<?php
require_once('begin.inc.php');
if(!is_connect()){
    header('Location:index.php');
    exit;
}
if($_POST['mode'] === 'taxo'){
    $taxonomy = new Taxonomy;
    $taxonomy->insert($_POST['name']);
    $tabTaxonomy = $taxonomy->getLevel0();
    ob_start();
    foreach ($tabTaxonomy as $list){
        include 'view/form/listTaxonomy.php';
    }
    $listTaxonomy = ob_get_contents();
    ob_end_clean();
    ob_start();
    foreach ($tabTaxonomy as $list){
        include 'view/form/optionTaxonomy.php';
    }
    $optionTaxonomy = ob_get_contents();
    $id = $list['id'];
    ob_end_clean();
    $data = [$listTaxonomy,$optionTaxonomy,$id];
    header('Content-Type: application/json');
    echo json_encode($data);
}elseif ($_POST['mode'] === 'layer') {
    $layer = new Layer;
    $layer->set('color',$_POST['color']);
    $layer->set('data_json',$_POST['geojson']);
    $layer->set('name',$_POST['name']);
    $layer->set('osm_tag',$_POST['osm_tag']);
    $layer->set('osm_wiki_link',$_POST['osm_wiki_link']);
    $layer->set('technical_comment',$_POST['technical_comment']);
    $layer->set('visible_by_default', $_POST['visible_by_default']);
    $layer->set('source', $_POST['source']);
    $layer->set('ovsql', $_POST['ovsql']);
    $layer->insert($_POST['idTaxonomy']);
    if($_POST['source'] === 'OSM'){
        $layer->updateTag($_POST['tag']);
    }
    // Rechargement de la liste des layers
    $tabLayer = $layer->SelectAll();
    ob_start();
    foreach ($tabLayer as $id_taxo => $table){
        echo "<div data-idTaxo ='$id_taxo' class='layerByTaxo'>";
        echo "<h4 id='taxoTitle-".$id_taxo."'>".$table['titre']."</h4>";
        foreach ($table['layer'] as $layer){
            include 'view/form/listLayer.php';
        }
        echo '</div>';
    }
    $listLayer = ob_get_contents();
    ob_end_clean();
    $data = [$listLayer,$_POST['idTaxonomy']];
    header('Content-Type: application/json');
    echo json_encode($data);
}elseif ($_POST['mode'] === 'user') {
    $user = new User;
    $user->setFromTab($_POST);
    $data = [];
    $data['res'] = $user->insert();
    $data['id'] = $user->get('id');
    $data['name'] = $user->get('name');
    header('Content-Type: application/json');
    echo json_encode($data);
}
