----------------
---mapfactory---
----------------

insert into config values('overpass_delay_many_request', 300);


----------------
-------mf-------
----------------
CREATE TABLE layer_internal_data (
id_layer int(11) primary key not null,
geojson longtext not null
) ;


CREATE TABLE cache (
id_layer int(11) primary key not null,
osm_data longtext not null
) ;


ALTER TABLE `layer` ADD `color` VARCHAR( 6 ) NOT NULL ;

ALTER TABLE `layer` ADD `source` VARCHAR( 10 ) NOT NULL ;
UPDATE `layer` SET source = 'OSM' WHERE 1

