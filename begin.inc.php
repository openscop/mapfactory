<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

session_start();
define('DEFAULT_LANGUAGE', 'fr_FR.UTF-8');  // fr_FR.UTF-8 or en_EN.UTF-8

// Set locale language
if(isset($_SESSION['lang'])){
$lang = !is_null($_SESSION['lang']) ? $_SESSION['lang'] : DEFAULT_LANGUAGE;
}else{
    $lang = DEFAULT_LANGUAGE;
}
putenv("LC_ALL=$lang");
setlocale(LC_ALL, $lang);
bindtextdomain($lang, './lang');
bind_textdomain_codeset($lang, "UTF-8");
textdomain($lang);
require_once('config.inc.php');
require_once('function.inc.php');
// Verification connexion
require_once('lib/session.php');
$btnConnect = btnConnexion();