<?php
require_once('instances/_global/config.php');  // Load configuration written by the installer
require_once('model/Instance.class.php');  
require_once('model/Layer.class.php');  
require_once('model/Cache.class.php');  

	define('INSTANCE_NAME_TECH', $_SERVER['argv'][1]);   // technical instance name
	define('DB_INSTANCE_HOST', $_SERVER['argv'][2]);
	define('DB_INSTANCE_DATABASE', $_SERVER['argv'][3]);
	define('DB_INSTANCE_USERNAME', $_SERVER['argv'][4]);
	define('DB_INSTANCE_PASSWORD', $_SERVER['argv'][5]);

	define('PATH_ROOT', DB_INSTANCE_HOST.SUBFOLDER); // absolute path of html files on the server without final '/'
	define('PATH_DATA', PATH_ROOT.'/instances/'.INSTANCE_NAME_TECH); // absolute path of instance data files on the server without final '/'
	//define('URL_DATA', SITE_URL.'/'.INSTANCE_NAME_TECH); // Url of instance with 'http://' and without final '/' > ex : http://www.domaine.tld/instance
	define('PATH_LOG', PATH_DATA.'/log'); // absolute path of instance log files on the server without final '/'
	$instance = new Instance();
	$cache = new Cache();
	$cache->set_state(false);	
	$cache->update();

