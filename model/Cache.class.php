<?php

/**
 * Created by Alois
 * Date: 15/06/2016
 * Time: 17:02
 */

class Cache
{

	/**
	 * 
	 */

	function __construct()
	{
		try {
			$this->db = new PDO("mysql:host=" . DB_INSTANCE_HOST . ";dbname=" . DB_INSTANCE_DATABASE, DB_INSTANCE_USERNAME, DB_INSTANCE_PASSWORD);
		} catch (PDOException $e) {
		    if ($handle = fopen(PATH_LOG . "/bd.log", "a+")) {
		        fwrite($handle, $e->getMessage());
		        fclose($handle);
		    }
		    die();
		}
		
		if (!isset($_SESSION['work_with_cache']))
		{
			$_SESSION['work_with_cache'] = true;
		}

		$this->db->query("SET NAMES UTF8");
	}	
	
	/**
	 * @return string json : state of cache
	 */

	public function get_state()
	{
		return '{"cache" : "'.$_SESSION['work_with_cache'].'"}';
	}

	/**
	 * set state of cache
	 */

	public function set_state($state)
	{	
		$_SESSION['work_with_cache'] = $state;
	}

	/**
	 * @return array contenant tout les ids des layers ( ou couches)
	 */
	
	private function get_all_layer_id()
	{
		$ids = array();
		$req = $this->db->prepare("SELECT id FROM layer WHERE source = 'OSM';");
		$req->execute();
		$res = $req->fetchAll(PDO::FETCH_OBJ);
		foreach ($res as $value)
		{
           		array_push($ids, $value->id);	
           	}
		return $ids;
	}			

	/**
	 * @return un tableau contenant tout les ids des layers ( ou couches) contenu dans le cache 
	 */

	private function get_all_cache_id()
	{
		$ids = array();
		$req = $this->db->prepare("SELECT id_layer FROM cache;");
		$req->execute();
		$res = $req->fetchAll(PDO::FETCH_OBJ);
		foreach ($res as $value)
		{
           		array_push($ids, $value->id_layer);
         	}
		return $ids;
	}
	
	/**
	 * update_cache : only update structure
	 */

	private function update_structure()
	{
		$ids_cache = $this->get_all_cache_id();				
		$ids_layer = $this->get_all_layer_id();	
		$missing_ids = array_diff($ids_layer, $ids_cache);
		$spare_ids = array_diff($ids_cache, $ids_layer);

		foreach ($missing_ids as $id) //add
		{
			$req = $this->db->prepare("INSERT INTO cache VALUES('".$id."','null');");
			$req->execute();
		}

		foreach ($spare_ids as $id) // remove
		{
			$req = $this->db->prepare("DELETE FROM cache WHERE id_layer = '".$id."';");
			$req->execute();
		}
	}
	
	/**
	 * update_cache : only update data of one layer
	 */

	private function update_data($id_layer) 
	{
		$layer = new Layer($id_layer);
		$data = $layer->get_data();
		$data = str_replace("'", "\'", $data);
        $data = str_replace('"', '\"', $data);
		if ($data == "error: no data found" || strlen($layer->get_data()) == 5)
		{
			echo 'Layer '.$layer->get_id().' - length : '.strlen($layer->get_data()).' error !'.PHP_EOL;
		}
		else
		{
			$req = $this->db->prepare("UPDATE cache SET osm_data = '".$data."' WHERE id_layer = '".$layer->get_id()."';");
			$req->execute();
		
			echo 'Layer '.$layer->get_id().' - length : '.strlen($layer->get_data()).PHP_EOL;
		}
	}

	/**
	 * update_all_data 
	 */

	private function update_all_data()
	{
		global $global_settings;
		$ids = $this->get_all_layer_id();
		foreach ($ids as $id)
		{
			$this->update_data($id);
			sleep($global_settings['overpass_delay_many_request']); // to avoid 429 error from overpass api
		}
	}

	/**
	 * update_cache : structure and data 
	 */

	public function update() // temps d'execution important du a la limite entre les requetees d'overpass 429 
	{
		$begin = time();

		//core

		$this->update_structure();
		$this->update_all_data();

		//core

		$end = time();
		$execution_time = $end - $begin;
		$execution_time_min = $execution_time/60;
		echo "all : ".$execution_time." sec soit environ ".$execution_time_min." minutes";
	} 
}
