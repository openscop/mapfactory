<?php

/**
 * Created by David
 * Date: 05/06/2015
 * Time: 16:29
 */
class Taxonomy
{

    private $db;


    /**
     *
     */
    function __construct()
    {
        try {
            $this->db = new PDO("mysql:host=" . DB_INSTANCE_HOST . ";dbname=" . DB_INSTANCE_DATABASE, DB_INSTANCE_USERNAME, DB_INSTANCE_PASSWORD);
        } catch (PDOException $e) {
            if ($handle = fopen(PATH_LOG . "/bd.log", "a+")) {
                fwrite($handle, $e->getMessage());
                fclose($handle);
            }
            die();
        }
        $this->db->query("SET NAMES UTF8");
    }

    /**
     * Execute an SELECT SQL request to load the level 0 items.
     * @return array : all terms
     */
    public function getLevel0()
    {
        $req = $this->db->prepare("SELECT * FROM `taxonomy_term` WHERE `parent` IS NULL ORDER BY `order`;");
        $req->execute();
        $res = $req->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }


    /**
     * Execute an SELECT SQL request to load the level 0 items.
     * @return array : all terms
     */
    public function foundRelation($id_term, $refName)
    {
        $req = $this->db->prepare("SELECT `id_" . $refName . "` FROM `" . $refName . "_taxonomy` WHERE `id_taxonomy` = $id_term;");
        $req->execute();
        $res = $req->fetchAll(PDO::FETCH_COLUMN);
        return $res;
    }
    
    
    /////////////////////////////////////////////////////////////////////////////
    public function deleteById($id){
        // Suprimer une ligne de la bdd, retourne true si réussi, false sinon
        $req = $this->db->prepare("DELETE FROM `taxonomy_term` WHERE `id` = :id");
        $req->execute([':id' => $id]);
        if($req->rowCount() > 0){
            return true;
        }
        return false;
    }
    
    public function insert($name) {
        // Ajouter une nouvelle ligne à la bdd
        $req = $this->db->prepare("INSERT INTO `taxonomy_term` SET `name` = :name");
        $req->execute([':name' => $name]);
    }
    
    public function update($id,$name){
        // Maj d'une ligne de la bdd
        $req = $this->db->prepare("UPDATE `taxonomy_term` SET `name` = :name WHERE `id` = :id");
        $req->execute([':id' => $id,':name'=> $name]);
        if($req->rowCount() === 1){
            return true;
        }
        return false;
    }
}
