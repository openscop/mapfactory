<div id="Taxo-<?=$list['id']?>">
    <button class="seeLayer btn btn-primary btn-sm" id="<?=$list['id']?>" title="Voir les layers"><i class="fas fa-list"></i></i></i></button>
    <button class="deleteTaxo btn btn-danger btn-sm" id="btnTaxo-<?=$list['id']?>" data-name="<?=htmlentities($list['name'])?>" title="suprimer"><i class="fas fa-trash-alt"></i></button>
    <button class="modifTaxo btn btn-warning btn-sm" id="btnTaxo-<?=$list['id']?>" title="modifier"><i class="far fa-edit"></i></button>
    <span><?=htmlentities($list['name'])?></span>
</div>