<!DOCTYPE html>

<html>
    <head>
        <title>Gestion la base de donnée</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="lib/leaflet/leaflet.css" rel="stylesheet" type="text/css"/>
        <link href="lib/font-awesome/css/all.css" rel="stylesheet" type="text/css"/>
        <<link href="lib/bootstrap/css/bootstrap.min.4.4.1.css" rel="stylesheet" type="text/css"/>
        <link href="css/cssform.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>          
        <header>    
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">MapFactory</a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php"><i class="fas fa-arrow-left"></i> Retour</a>
                        </li>
                    </ul>  
                </div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#user">Utilisateurs <i class="fas fa-users"></i></a>
                    </li>
                </ul>
            </nav>
        </header>
        <main class="container">
            <div class='msg alert alert-primary'>  
            </div>
            <div class="row">
                <div class="taxonomie col-md-6 col-sm-12">
                    <div>
                        <h2>Catégorie</h2>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Nom de la catégorie" id="name">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" id="addTaxo"><i class="fas fa-plus"> Créer</i></button>
                            </div>
                        </div>
                        <hr>
                        <div class="list">
                            <?=$listTaxonomy?>
                        </div>               
                    </div>
                </div>
                <div class="layer col-md-6 col-sm-12">
                    <h2>Layer</h2>
                    <button id="listLayer" class="btn btn-primary"><i class="fas fa-list"></i> Liste des layers</button>
                    <button id="newLayer" class="btn btn-success"><i class="fas fa-plus"></i> Nouveau layer</button>
                    <hr>
                    <div class="layerList">
                        <?=$listLayer?>
                    </div>
                    <div class="newLayer">
                        <div class="save">
                            <button id="validLayer" class="btn btn-success btn-sm"><i class="fas fa-check"></i> Enregistrer</button>
                            <button id="cancelLayer" class="btn btn-danger btn-sm"><i class="fas fa-times"></i> Annuler</button>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-7">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Nom</span>
                                    </div>
                                    <input type="text" class="form-control" id="nameLayer">
                                </div>
                            </div>    
                            <div class="col-5">
                                <div>
                                    <input type="checkbox" id="visibility"/>
                                    <label for="source" >Visible par défaut</label>                                   
                                </div>
                                <div>
                                    <label for="color">Couleur </label>
                                    <input type="color" id="color"/>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="selectTaxonomy">Catégorie</label>
                            </div>
                            <select class="custom-select" id="selectTaxonomy">
                                <?=$optionTaxonomy?>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Comentaires</span>
                            </div>
                            <textarea class="form-control" id="technical_comment"></textarea>
                        </div>
                        <br>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="source">Source</label>
                            </div>
                            <select class="custom-select" id="source">
                                <option value="internal">Interne</option>
                                <option value="OSM">OpenStreetMap</option>
                            </select>
                        </div>
                        <hr>                        
                        <div class="osm">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">OSM wiki</span>
                                </div>
                                <textarea class="form-control" id="osm_wiki_link"></textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">OSM tag</span>
                                </div>
                                <textarea class="form-control" id="osm_tag"></textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Overpassql</span>
                                </div>
                                <textarea class="form-control" id="overpassql"></textarea>
                                <button class="btn btn-primary" id="seeOsmData" data-idLayer="0">Voir</button>
                            </div>
                            <br>
                        </div>
                        <div class="interne">                              
                                <div id="divMap">
                                    <div id="mapid"></div>
                                    <button class="btn btn-light float-right" id="resize"><i class="fas fa-expand"></i></button>
                                    <button class="btn btn-info btn-lg" id="newDraw">Nouveau Tracé</button>
                                    <div class="btn-group-vertical float-left" id="toolBar">
                                        <button class="btn btn-info btn-lg" id="toolHand"><i class="far fa-hand-point-up"></i></button>
                                        <button class="btn btn-dark btn-lg" id="toolPoint"><i class="fas fa-map-marker-alt"></i></button>
                                        <button class="btn btn-dark btn-lg" id="toolLine"><i class="fas fa-project-diagram"></i></button>
                                        <button class="btn btn-dark btn-lg" id="toolPoly"><i class="fas fa-draw-polygon"></i></button>
                                    </div>
                                </div>
                            <div id="tagsPannel"></div>
                        </div>
                    </div>
                </div>
            </div>
        </main>  
        <div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Gestion des utilisateurs</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="list">
                        <h5>Liste des utilisateurs</h5>
                        <hr>
                        <?=$listUser?>
                    </div>
                    <div class="modal-body">
                        <h5>Nouvel utilisateur <span id="msgFormUser" class="h6"></span></h5>
                        <hr>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom</span>
                            </div>
                            <input type="text" id="userName">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Password</span>
                            </div>
                            <input type="password" id="password">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Password</span>
                            </div>
                            <input type="password" id="password2">                           
                        </div>
                        <button type="button" class="btn btn-success float-right" id="addUser"><i class="fas fa-plus"> Créer</i></button>
                    </div>                  
                </div>
            </div>
        </div>  
       <script src="lib/leaflet/leaflet.js" type="text/javascript"></script>
       <script src="lib/jquery/jquery-3.4.1.js" type="text/javascript"></script>
       <script src="lib/bootstrap/js/bootstrap.min.4.4.1.js" type="text/javascript"></script>
       <script src="js/osmtogeojson.js" type="text/javascript"></script>
       <script src="js/form.js" type="text/javascript"></script>
    </body>
</html>