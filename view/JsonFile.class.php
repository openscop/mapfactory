<?php
/**
 * Json.class.php
 * Created by David.
 * Date: 03/06/2015 10:12
 *
 *
 * This class generate a Json file.
 *
 * It requires one parameter : the json content.
 *
 *
 */

class JsonFile
{
    private $jsonContent ;

    /**
     *
     */
    function __construct()
    {

    }


    public function addContent($jsonContent)
    {
        $this->jsonContent = $jsonContent;
    }

    /**
     * @return string : The HTML content
     */
    public function render()
    {
        header('Content-Type: application/json');
        echo $this->jsonContent;

/*        ob_start();
        require(PATH_ROOT.'/view/page.inc.phtml');
        $str = ob_get_contents();
        ob_end_clean();
        return $str;*/
    }

}