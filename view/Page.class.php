<?php
/**
 * Page.class.php
 * Created by David.
 * Date: 29/05/13 10:12
 *
 *
 * This class generate a HTML page.
 *
 * It requires one parameter : the name of page.
 *
 *
 */

class Page
{

    public $format = "html";
    public $about = "";
    private $name = null;
    private $jsFile = array();
    private $cssFile = array();
    private $jsVariable = array();
    private $cssCode = "";
    private $body = array();

    /**
     * @param $pageName : This page name is displayed in title of the HTML page.
     *
     */
    function __construct($pageName)
    {
        $this->name = $pageName;
    }

    /**
     * @param $cssName : The name of one CSS file without extension '.css' and stored in '/css' directory
     *
     * Adds this css file in the <head> of the HTML page.
     *
     */
    public function addCssFile($cssFileName)
    {
        $this->cssFile[] = $cssFileName;
    }

    /**
     * @param $js : The name of one JavaScript file without extension '.js' and stored in '/js' directory
     *
     * Adds this js file in the <head> of the HTML page.
     */
    public function addJsFile($jsFileName)
    {
        $this->jsFile[] = $jsFileName;
    }

    public function addJsVariable($name, $value)
    {
        $this->jsVariable[$name] = $value;
    }

    public function getJsVariable()
    {
        $str = null;
        foreach($this->jsVariable as $name => $value) {
            $str .= (is_null($str)) ? "{" : ",";
            if (!isJSON($value)) {
                $value = '"' . addcslashes($value, '"') . '"'; // add double quote if value is not a json
            }
            $str .= "\"$name\": $value";
        }
        $str .= "}";
        return json_encode($str);
    }
    /**
     * @param $cssName : The name of one CSS file without extension '.css' and stored in '/css' directory
     *
     * Adds this css file in the <head> of the HTML page.
     *
     */
    public function addCssCode($cssCode)
    {
        $this->cssCode .= $cssCode;
    }


    /**
     * @param $viewName : The name of the view who generates the content
     * @param $content : The variable who stores the data for content generator of the view
     *
     * Adds one content in the <body> of the HTML page.
     *
     */
    public function addContentBodyByView($viewName, $content)
    {
        $this->body[] = array('type' => 'view', 'viewName' => $viewName, 'content' => $content);
        if (file_exists(PATH_ROOT . '/css/' . $viewName . '.css')) {
            $this->addCssFile($viewName);
        }
        if (file_exists(PATH_ROOT . '/js/' . $viewName . '.js')) {
            $this->addJsFile($viewName);
        }
    }

    /**
     * @param $viewName : The name of the view who generates the content
     * @param $content : The variable who stores the data for content generator of the view
     *
     * Adds one content in the <body> of the HTML page.
     *
     */
    public function addContentBodyByCode($html)
    {
        $this->body[] = array('type' => 'code', 'html' => $html);
    }

    /**
     * @return string : The HTML content
     */
    public function render()
    {
        ob_start();
        require(PATH_ROOT.'/view/page.inc.phtml');
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }

}
