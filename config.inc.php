<?php
define('VERSION', '0.6.0'); // version number of this application
require_once('instances/_global/config.php');  // Load configuration written by the installer

// BOF Detection of the server configuration
if (isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT']<>"") {
    define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']); // absolute path of apache DOCUMENT_ROOT '/'
}
if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']<>"") {
    define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST'].SUBFOLDER); // Url of MapFactory application with 'http://' and without final '/' > ex : http://www.domaine.tld{
}
define('PATH_ROOT', DOC_ROOT.SUBFOLDER); // absolute path of html files on the server without final '/'
define('PATH_GLOBAL_LOG', PATH_ROOT.'/instances/_global/log'); // absolute path of global log files on the server without final '/'
// EOF Server configuration

// BOF Retrieving global settings of the application from the database

try {
    $global_db = new PDO("mysql:host=" . DB_GLOBAL_HOST . ";dbname=" . DB_GLOBAL_DATABASE, DB_GLOBAL_USERNAME, DB_GLOBAL_PASSWORD);
} catch (PDOException $e) {
    
    if ($handle = fopen(PATH_GLOBAL_LOG."db.log", "a+")) {
        fwrite($handle, $e->getMessage());
        fclose($handle);
    }
    die();
    $this->db->query("SET NAMES UTF8");
}
$req = $global_db->prepare("SELECT * FROM config WHERE 1;");
$req->execute();
$res = $req->fetchAll(PDO::FETCH_OBJ);
foreach ($res as $value) {
    $global_settings[$value->name] = $value->data;
}
// EOF Retrieving global settings of the application from the database
// BOF Retrieving the global list of instances from the database
define('INSTANCE_DOMAIN', parse_url(SITE_URL, PHP_URL_HOST));   // domain name of the instance requested by the user
$req = $global_db->prepare("SELECT * FROM instances WHERE domain = '". INSTANCE_DOMAIN ."';");
$req->execute();
$instance_settings = $req->fetch(PDO::FETCH_OBJ);
// BOF Retrieving the global list of instances from the database

// BOF Set the instance parameters
define('INSTANCE_NAME_TECH', $instance_settings->tech_name);   // technical instance name
define('DB_INSTANCE_HOST','localhost');
define('DB_INSTANCE_DATABASE',$instance_settings->db_name);
define('DB_INSTANCE_USERNAME',$instance_settings->db_user);
define('DB_INSTANCE_PASSWORD',$instance_settings->db_pw);
define('PATH_DATA', PATH_ROOT.'/instances/'.INSTANCE_NAME_TECH); // absolute path of instance data files on the server without final '/'
define('URL_DATA', SITE_URL.'/'.INSTANCE_NAME_TECH); // Url of instance with 'http://' and without final '/' > ex : http://www.domaine.tld/instance
define('PATH_LOG', PATH_DATA.'/log'); // absolute path of instance log files on the server without final '/'
unset($instance_settings);
// EOF Set the instance parameters

// Other application settings
define ("EXT_IMAGE", serialize (array ("jpg", "jpeg", "png")));  # allowed extensions, serialize array, use it with > $data = unserialize (EXT_IMAGE);
define ("EXT_VIDEO", serialize (array ("mp4", "mpg", "mpeg", "mov")));  # allowed extensions, serialize array, use it with > $data = unserialize (EXT_VIDEO);
// End of Other application settings
