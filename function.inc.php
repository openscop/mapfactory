<?php
/**
 * @param $name : The name of the class is called to create a new instance
 *
 * Automatic loading of class file
 */
function __autoload($name)
{
    if (file_exists(PATH_ROOT . "/model/" . $name . ".class.php")) {
        require_once(PATH_ROOT . "/model/" . $name . ".class.php");
    } elseif (file_exists(PATH_ROOT . "/view/" . $name . ".class.php")) {
        require_once(PATH_ROOT . "/view/" . $name . ".class.php");
    } else {
        echo _("Unable to open to ") . $name . ".class.php";
    }
}


/**
 * @param $a
 * @param $b
 * @return int
 *
 * This fonction is used by usort to ordering data by 'order' column.
 */
function sortByOrder($a, $b)
{
    if ($a['order'] == $b['order']) {
        return 0;
    }
    return ($a['order'] < $b['order']) ? -1 : 1;
}

/**
 * Translate a result array into a HTML table
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.3.2
 * @link        http://aidanlister.com/2004/04/converting-arrays-to-human-readable-tables/
 * @param       array $array      The result (numericaly keyed, associative inner) array.
 * @param       bool $recursive  Recursively generate tables for multi-dimensional arrays
 * @param       string $null       String to output for blank cells
 */
function array2table($array, $recursive = false, $null = '&nbsp;')
{
    // Sanity check
    if (empty($array) || !is_array($array)) {
        return false;
    }

    if (!isset($array[0]) || !is_array($array[0])) {
        $array = array($array);
    }

    // Start the table
    $table = "<table border='1px'>\n";

    // The header
    $table .= "\t<tr>";
    // Take the keys from the first row as the headings
    foreach (array_keys($array[0]) as $heading) {
        $table .= '<th>' . $heading . '</th>';
    }
    $table .= "</tr>\n";

    // The body
    foreach ($array as $row) {
        $table .= "\t<tr>";
        foreach ($row as $cell) {
            $table .= '<td>';

            // Cast objects
            if (is_object($cell)) {
                $cell = (array)$cell;
            }

            if ($recursive === true && is_array($cell) && !empty($cell)) {
                // Recursive mode
                $table .= "\n" . array2table($cell, true, true) . "\n";
            } else {
                $tooltip = "gettype(): " . strtoupper(gettype($cell));
                $table .= '<span title="' . $tooltip . '">' . ((strlen($cell) > 0) ?
                        htmlspecialchars((string)$cell) : $null) . '</span>';
            }

            $table .= '</td>';
        }

        $table .= "</tr>\n";
    }

    $table .= '</table>';
    return $table;
}

/**
 * @param $string String to check
 * @return boolean
 *
 * This fonction check if a string is valid JSON
 */
function isJSON($string){
    return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

/**
 * @param $string String to clean
 * @return $string String cleaned
 *
 * This function removes the line breaks, tabulations and unnecessary spaces
 */
function trimAnywhere($string){
    if (is_string($string)) {
        $string = trim($string);
        $string = str_replace("\t", " ", $string);
        $string = str_replace("\r", " ", $string);
        $string = str_replace("\n", " ", $string);
        $string = preg_replace("( +)", " ", $string);
        return $string;
    }
}
